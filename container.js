const fs = require('fs')
module.exports = class Contenedor {
    constructor(archivo) {
        this.archivo = archivo;
    }
    async save(object) {
        const allobjects = await this.getAll()
        let newId = 1
        if (allobjects.length != 0) {
            newId = allobjects[allobjects.length - 1].id + 1
        }
        const newObject = { ...object, id: newId }
        allobjects.push(newObject)
        try {
            await fs.promises.writeFile(`${__dirname}/${this.archivo}`, JSON.stringify(allobjects, null, 2))
            return console.log("Se ha guardado correctamente :")
        } catch (error) {
            throw new Error(`Lo sentimos :${error}`)
        }
    }
    async getById(id) {
        const allobjects = await this.getAll()
        if (allobjects.length == 0) {
            console.log(`Lo sentimos no se ha encontrado el id: ${id}`)
        } else {
            return allobjects.find(objetc => objetc.id == id ? objetc : console.log(`Lo sentimos no se ha encontrado el id: ${id}`))
        }
    }
    async getAll() {
        try {
            //console.log("getAll", `${__dirname}/${this.archivo}`)
            const allobjects = await fs.promises.readFile(`${__dirname}/${this.archivo}`, 'utf-8')
            return JSON.parse(allobjects)
        } catch (error) {
            return []
        }
    }
    async deleteById(id) {
        const allobjects = await this.getAll()
        const index = allobjects.findIndex(objetc => objetc.id == id)
        try {
            if (index == -1) {
                throw new Error(`Lo sentimos no se ha encontrado el id: ${id}`)
            } else {
                allobjects.splice(index, 1)
                await fs.promises.writeFile(this.archivo, JSON.stringify(allobjects, null, 2))
            }
        } catch (error) {
            throw new Error(`Lo sentimos : ${error}`)
        }
    }
    async deleteAll() {
        await fs.promises.writeFile(this.archivo, JSON.stringify([], null, 2))
    }
}