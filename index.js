const express = require('express');
const path = require('path');
let Contenedor = require('./container.js');

const aplicacion = express();

let controlador = new Contenedor("products.txt")

const servidor = aplicacion.listen(8080, () => {
    console.log("servidor escuchando en puerto " + 8080);
})

servidor.on("error", (err) => {
    console.log("error", err);
})

aplicacion.get("/", (resquest, res) => {
    res.send("<h1>Bienvenidx!</h1><ul><li>Lista de productos completa en /productos</li><li>Producto random en la ruta /productoRandom</li></ul>");
})

aplicacion.get('/productos', (req, res) => {
    controlador.getAll().then(data => {
        res.send(data);
    })

});

aplicacion.get('/productoRandom', (req, res) => {
    controlador.getById(Math.floor(Math.random() * 3) + 1).then(response => {
        res.send(response);
    })
});